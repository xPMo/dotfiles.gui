#!/usr/bin/env bash
IFS=$'\n\t'
shutdown(){
	# word splitting
	#shellcheck disable=2046
	kill $(ps -o pid= --ppid $$)
	exit 0
}

pipe="$(mktemp -u)"
mkfifo -m 600 "$pipe"
# playerctl loop on fd 5
while sleep 15; do
	playerctl --follow metadata --format
		$'{{status}}\t{{position}}\t{{mpris:length}}\t{{artist}} - {{title}} {{duration(position)}}|{{duration(mpris:length)}}'
done > "$pipe" &

while read -r status position length line; do
	# escape [&<>] for pango formatting, json escaping
	line="${line//&/&amp;}"
	line="${line//>/&gt;}"
	line="${line//</&lt;}"
	line="${line//\"/\\\"}"
	percentage="$(( (100 * position) / length ))"
	case $status in
		Paused) text='"<span foreground=\"#cccc00\" size=\"smaller\">'"$line"'</span>"' ;;
		Playing) text=\""<small>$line</small>"\" ;;
		*)text='"<span foreground="#073642">⏹</span>"' ;;
	esac
	printf '%s\n' '{"text":'"$text"',"tooltip":"'"$status"'","class":"'"$percentage"'","percentage":'"$percentage"'}'
done < "$pipe" &
