#!/usr/bin/env bash
[[ -n "${1:-}" ]] && alias light="light -s $1"
final=$(light -r)
trap '{ echo; light -Sr $final; exit $?; }' INT
while true; do
	light -S 2
	sleep 0.015
	light -S 100
	sleep 0.015
done
