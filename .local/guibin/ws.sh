#!/usr/bin/env bash
. "$HOME/.local/lib/i3/i3tool.sh"
case ${1,,} in
	t*) i3tool msg workspace 1:1 , \
		msg -- exec nvim-gtk "$HOME/Documents/current/repos/topology-homework/main.tex" ,\
		msg -- exec zathura "$HOME/Documents/current/Topology/Introduction\\ to\\ Metric\\ and\\ Topological\\ Spaces.pdf"
	;;
esac
